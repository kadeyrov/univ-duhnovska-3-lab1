//
//  main.cpp
//  lab1
//
//  Created by Kadir Kadyrov on 19.03.2021.
//

#include <iostream>
#include <vector>

using namespace std;

void multiplyMatrix(
             int m,
             int n,
             int p,
             vector<vector<int>> a,
             vector<vector<int>> b,
             vector<vector<int>> &c
             ) {
    
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            
            c[i][j] = 0;
            for (int k = 0; k < p; ++k)
                c[i][j] += a[i][k] * b[k][j];
        }
    }
}

void scanMatrix(
                int n,
                int m,
                vector<vector<int>> &a
                ) {
    
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            cin >> a[i][j];
}

void genRandomMatrix(
                     int n,
                     int m,
                     vector<vector<int>> &a
                     ) {
    
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            a[i][j] = rand() % 2000 - 1000;
}

void printMatrix(
                     int n,
                     int m,
                     vector<vector<int>> a
                     ) {
    
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++)
            cout << a[i][j] << ' ';
        cout << endl;
    }
}

int main(int argc, const char * argv[]) {
    
    int m, n, p;
    vector<vector<int>> a, b, ans;
    
    cin >> m >> n >> p;
    
    a = vector<vector<int>>(m, vector<int>(p));
    b = vector<vector<int>>(p, vector<int>(n));
    ans = vector<vector<int>>(m, vector<int>(n));
    
    if (m < 3 && n < 3 && p < 3) {
        scanMatrix(m, p, a);
        scanMatrix(p, n, b);
    } else {
        genRandomMatrix(m, p, a);
        genRandomMatrix(p, n, b);
        
        printMatrix(m, p, a);
        cout << endl << endl;
        printMatrix(p, n, b);
        cout << endl << endl;
    }
    
    multiplyMatrix(m, n, p, a, b, ans);
    printMatrix(m, n, ans);
}
